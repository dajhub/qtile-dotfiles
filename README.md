# Qtile dotfiles

My personal dotfiles for qtile installation.

## Image

![qtile - Arch](assets/qtile.png)

## Installation

These are reminder notes to myself...

Started with an arch install using the command `archinstall`. Chose the **qtile** desktop and **ly** as a display manager.

Additional packages installed using paru/yay:
```
alacritty
alsa-utils # Needed for bar
betterlockscreen
brightnessctl # Needed for brightness control in bar
dmenu
dunst
engrampa # Archive manager for pcmanfm file manager
fish
flameshot # Screenshot utility
flatpak 
htop
inter-font
lxappearance
micro
nitrogen # Setting wallpapwer
otf-noto-sans-cjk
pcmanfm
picom
python-psutil # Needed for volume control in bar
qtile-extras
rofi
ttf-font-awesome
viewnior # Image viewer
vivaldi
xautolock # Needed for lock screen
xclip # Needed this for copying from micro editor to another application
xsel  # Needed this for copying from micro editor to another application
```

In the downloaded .config files, possible changes before restarting qtile:
1. In the **picom.conf** file under *General Settings* may need to change **glx** to **xrendr** or vice versa.  There have been occasions where, when set wrong, screen was freezing and I was unable to enter text.
2. In `/.config/qtile/scripts/autostart.sh` the monitors may need to be changed.  It is currently set for a dual monitor.  Monitor(s) can be found by running `xrandr`.
