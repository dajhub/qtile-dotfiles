if status is-interactive
    # Commands to run in interactive sessions can go here
end

function fish_greeting
  set_color $fish_color_autosuggestion
  echo "Welcome..."
  set_color normal
end

export "MICRO_TRUECOLOR=1"

export PATH="$PATH:/home/$USER/.local/bin"

# OH-MY-FISH SETTINGS
# Installed oh-my-fish:
# curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install | fish
# then the theme Bob-the-fish: https://github.com/oh-my-fish/oh-my-fish/blob/master/docs/Themes.md#bobthefish

set -g theme_color_scheme solarized-dark
set -g theme_display_date no
set -g theme_title_use_abbreviated_path  no
set -g theme_nerd_fonts yes
