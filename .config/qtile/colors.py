# If using transparency, make sure you add (background="#00000000") to 'Screen' line(s).
# Then, you can use RGBA color codes to add transparency to the colors below.
# For ex: colors = [["#282c34ee", "#282c34dd"], ...

CatppuccinLight = [
    ["eff1f5", "eff1f5"], # 00 base
    ["e6e9ef", "e6e9ef"], # 01 mantle
    ["ccd0da", "ccd0da"], # 02 surface0
    ["bcc0cc", "bcc0cc"], # 03 surface1
    ["acb0be", "acb0be"], # 04 surface2
    ["4c4f69", "4c4f69"], # 05 text
    ["dc8a78", "dc8a78"], # 06 rosewater
    ["7287fd", "7287fd"], # 07 lavender
    ["d20f39", "d20f39"], # 08 red
    ["fe640b", "fe640b"], # 09 peach
    ["df8e1d", "df8e1d"], # 10 yellow
    ["40a02b", "40a02b"], # 11 green
    ["179299", "179299"], # 12 teal
    ["1e66f5", "1e66f5"], # 13 blue
    ["8839ef", "8839ef"], # 14 mauve
    ["dd7878", "dd7878"]  # 15 flamingo
    ]

SelenizedDark = [
    ["103c48", "103c48"], # 00 Default Background
    ["184956", "184956"], # 01 Lighter Background (Used for status bars, line number and folding marks)
    ["2d5b69", "2d5b69"], # 02 Selection Background
    ["72898f", "72898f"], # 03 Comments, Invisibles, Line Highlighting
    ["72898f", "72898f"], # 04 Dark Foreground (Used for status bars)
    ["adbcbc", "adbcbc"], # 05 Default Foreground, Caret, Delimiters, Operators
    ["cad8d9", "cad8d9"], # 06 Light Foreground (Not often used)
    ["cad8d9", "cad8d9"], # 07 Light Foreground (Not often used)
    ["fa5750", "fa5750"], # 08 Red: Variables, XML Tags, Markup Link Text, Markup Lists, Diff Delete
    ["ed8649", "ed8649"], # 09 Orange: Integers, Boolean, Constants, XML Attributes, Markup Link Url
    ["dbb32d", "dbb32d"], # 10 Mustard: Classes, Markup Bold, Search Text Background
    ["75b938", "75b938"], # 11 Green: Strings, Inherited Class, Markup Code, Diff Inserted
    ["41c7b9", "41c7b9"], # 12 Turqoise: Support, Regular Expressions, Escape Characters, Markup Quotes
    ["4695f7", "4695f7"], # 13 Blue: Functions, Methods, Attribute IDs, Headings
    ["af88eb", "af88eb"], # 14 Purple: Keywords, Storage, Selector, Markup Italic, Diff Changed
    ["f275be", "f275be"]  # 15 Pink: Deprecated, Opening/Closing Embedded Language
    ]

SelenizedLight = [
    ["ffffff", "ffffff"], # 00 Default Background
    ["ebebeb", "ebebeb"], # 01 Lighter Background (Used for status bars, line number and folding marks)
    ["cdcdcd", "cdcdcd"], # 02 Selection Background
    ["878787", "878787"], # 03 Comments, Invisibles, Line Highlighting
    ["878787", "878787"], # 04 Dark Foreground (Used for status bars)
    ["474747", "474747"], # 05 Default Foreground, Caret, Delimiters, Operators
    ["282828", "282828"], # 06 Light Foreground (Not often used)
    ["282828", "282828"], # 07 Light Foreground (Not often used)
    ["bf0000", "bf0000"], # 08 Red: Variables, XML Tags, Markup Link Text, Markup Lists, Diff Delete
    ["ba3700", "ba3700"], # 09 Orange: Integers, Boolean, Constants, XML Attributes, Markup Link Url
    ["af8500", "af8500"], # 10 Mustard: Classes, Markup Bold, Search Text Background
    ["008400", "008400"], # 11 Green: Strings, Inherited Class, Markup Code, Diff Inserted
    ["009a8a", "009a8a"], # 12 Turqoise: Support, Regular Expressions, Escape Characters, Markup Quotes
    ["0054cf", "0054cf"], # 13 Blue: Functions, Methods, Attribute IDs, Headings
    ["6b40c3", "6b40c3"], # 14 Purple: Keywords, Storage, Selector, Markup Italic, Diff Changed
    ["dd0f9d", "dd0f9d"]  # 15 Pink: Deprecated, Opening/Closing Embedded Language
    ]


NordLight = [
    ["e5e9f0", "e5e9f0"], # 00 Default Background
    ["c2d0e7", "c2d0e7"], # 01 Lighter Background (Used for status bars, line number and folding marks)
    ["b8c5db", "b8c5db"], # 02 Selection Background
    ["aebacf", "aebacf"], # 03 Comments, Invisibles, Line Highlighting
    ["60728c", "60728c"], # 04 Dark Foreground (Used for status bars)
    ["2e3440", "2e3440"], # 05 Default Foreground, Caret, Delimiters, Operators
    ["3b4252", "3b4252"], # 06 Light Foreground (Not often used)
    ["29838d", "29838d"], # 07 Light Foreground (Not often used)
    ["99324b", "99324b"], # 08 Red: Variables, XML Tags, Markup Link Text, Markup Lists, Diff Delete
    ["ac4426", "ac4426"], # 09 Orange: Integers, Boolean, Constants, XML Attributes, Markup Link Url
    ["9a7500", "9a7500"], # 10 Mustard: Classes, Markup Bold, Search Text Background
    ["4f894c", "4f894c"], # 11 Green: Strings, Inherited Class, Markup Code, Diff Inserted
    ["398eac", "398eac"], # 12 Turqoise: Support, Regular Expressions, Escape Characters, Markup Quotes
    ["3b6ea8", "3b6ea8"], # 13 Blue: Functions, Methods, Attribute IDs, Headings
    ["97365b", "97365b"], # 14 Purple: Keywords, Storage, Selector, Markup Italic, Diff Changed
    ["5272af", "5272af"]  # 15 Pink: Deprecated, Opening/Closing Embedded Language
    ]


NordDark = [
    ["2e3440", "2e3440"], # 00 Default Background
    ["3B4252", "3B4252"], # 01 Lighter Background (Used for status bars, line number and folding marks)
    ["434C5E", "434C5E"], # 02 Selection Background
    ["4C566A", "4C566A"], # 03 Comments, Invisibles, Line Highlighting
    ["D8DEE9", "D8DEE9"], # 04 Dark Foreground (Used for status bars)
    ["E5E9F0", "E5E9F0"], # 05 Default Foreground, Caret, Delimiters, Operators
    ["ECEFF4", "ECEFF4"], # 06 Light Foreground (Not often used)
    ["8FBCBB", "8FBCBB"], # 07 Light Foreground (Not often used)
    ["BF616A", "BF616A"], # 08 Red: Variables, XML Tags, Markup Link Text, Markup Lists, Diff Delete
    ["D08770", "D08770"], # 09 Orange: Integers, Boolean, Constants, XML Attributes, Markup Link Url
    ["EBCB8B", "EBCB8B"], # 10 Mustard: Classes, Markup Bold, Search Text Background
    ["A3BE8C", "A3BE8C"], # 11 Green: Strings, Inherited Class, Markup Code, Diff Inserted
    ["88C0D0", "88C0D0"], # 12 Turqoise: Support, Regular Expressions, Escape Characters, Markup Quotes
    ["81A1C1", "81A1C1"], # 13 Blue: Functions, Methods, Attribute IDs, Headings
    ["B48EAD", "B48EAD"], # 14 Purple: Keywords, Storage, Selector, Markup Italic, Diff Changed
    ["5E81AC", "5E81AC"]  # 15 Pink: Deprecated, Opening/Closing Embedded Language
    ]



#base00: "103c48" # Default Background
#base01: "184956" # Lighter Background (Used for status bars, line number and folding marks)
#base02: "2d5b69" # Selection Background
#base03: "72898f" # Comments, Invisibles, Line Highlighting
#base04: "72898f" # Dark Foreground (Used for status bars)
#base05: "adbcbc" # Default Foreground, Caret, Delimiters, Operators
#base06: "cad8d9" # Light Foreground (Not often used)
#base07: "cad8d9" # Light Background (Not often used)
#base08: "fa5750" # Red: Variables, XML Tags, Markup Link Text, Markup Lists, Diff Deleted
#base09: "ed8649" # Orange: Integers, Boolean, Constants, XML Attributes, Markup Link Url
#base0A: "dbb32d" # Mustard: Classes, Markup Bold, Search Text Background
#base0B: "75b938" # Green: Strings, Inherited Class, Markup Code, Diff Inserted
#base0C: "41c7b9" # Turqoise: Support, Regular Expressions, Escape Characters, Markup Quotes
#base0D: "4695f7" # Blue: Functions, Methods, Attribute IDs, Headings
#base0E: "af88eb" # Purple: Keywords, Storage, Selector, Markup Italic, Diff Changed
#base0F: "f275be" # Pink: Deprecated, Opening/Closing Embedded Language
