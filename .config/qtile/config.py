# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# --------------------------------------------------------
# PACKAGE IMPORTS
# --------------------------------------------------------

import os
import subprocess
from libqtile import bar, extension, hook, layout, qtile, widget
from libqtile.config import Click, Drag, Group, Key, KeyChord, Match, Screen
from libqtile.lazy import lazy
# Make sure 'qtile-extras' is installed or this config will not work.
from qtile_extras import widget
from qtile_extras.widget.decorations import BorderDecoration
#from qtile_extras.widget import StatusNotifier
import colors



# --------------------------------------------------------
# PERSONAL VARIABLES
# --------------------------------------------------------

mod = "mod4"                            # Sets mod key to SUPER/WINDOWS
altmod = "mod1"                         # Sets the alt key
myTerm = "alacritty"                    # My terminal 
myBrowser = "vivaldi-stable"          # My browser 
myFileManager = "pcmanfm"             # My file manager


# --------------------------------------------------------
# SETUP LAYOUT THEME
# --------------------------------------------------------

colors = colors.CatppuccinLight

layout_theme = {"border_width": 2,
                "margin": 8,
                "border_focus": colors[12],
                "border_normal": colors[0]
                }

# --------------------------------------------------------
# CUSTOM FUNCTIONS
# --------------------------------------------------------


# A function for hide/show all the windows in a group
@lazy.function
def minimize_all(qtile):
    for win in qtile.current_group.windows:
        if hasattr(win, "toggle_minimize"):
            win.toggle_minimize()
           
# A function for toggling between MAX and MONADTALL layouts
@lazy.function
def maximize_by_switching_layout(qtile):
    current_layout_name = qtile.current_group.layout.name
    if current_layout_name == 'monadtall':
        qtile.current_group.layout = 'max'
    elif current_layout_name == 'max':
        qtile.current_group.layout = 'monadtall'

# --------------------------------------------------------
# KEYBINDINGS
# --------------------------------------------------------

keys = [
    # The essentials
    Key([mod], "Return", lazy.spawn(myTerm), desc="Terminal"),
    Key([mod], "d", lazy.spawn("rofi -show drun"), desc='Run Launcher'),
    Key([mod], "e", lazy.spawn(myFileManager), desc='Launches File Manager'),
    Key([mod], "w", lazy.spawn(myBrowser), desc='Launches Web browser'),
    Key([mod], "b", lazy.hide_show_bar(position='all'), desc="Toggles the bar to show/hide"),
    Key([altmod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "shift"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([altmod], "l", lazy.spawn("betterlockscreen --lock blur"), desc="Lock screen"),
    Key([mod], "p", lazy.spawn("sh -c ~/.config/rofi/scripts/power"), desc='powermenu'),
    Key([altmod], "d", lazy.spawn("sh -c ~/.config/bemenu/bemenu.sh"), desc='bemenu'),



    # Rotate through workspaces
    Key([mod,], "Tab", lazy.screen.next_group(skip_empty=True), "Switch to group to the right"),
    Key([mod], "grave", lazy.screen.prev_group(skip_empty=True), "Switch to group to the left"),

    
    # Switch between windows
    # Some layouts like 'monadtall' only need to use j/k to move
    # through the stack, but other layouts like 'columns' will
    # require all four directions h/j/k/l to move around.
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h",
        lazy.layout.shuffle_left(),
        lazy.layout.move_left().when(layout=["treetab"]),
        desc="Move window to the left/move tab left in treetab"),

    Key([mod, "shift"], "l",
        lazy.layout.shuffle_right(),
        lazy.layout.move_right().when(layout=["treetab"]),
        desc="Move window to the right/move tab right in treetab"),

    Key([mod, "shift"], "j",
        lazy.layout.shuffle_down(),
        lazy.layout.section_down().when(layout=["treetab"]),
        desc="Move window down/move down a section in treetab"
    ),
    Key([mod, "shift"], "k",
        lazy.layout.shuffle_up(),
        lazy.layout.section_up().when(layout=["treetab"]),
        desc="Move window downup/move up a section in treetab"
    ),


    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with multiple stack panes
    Key([mod, "shift"], "space", lazy.layout.toggle_split(), desc="Toggle between split and unsplit sides of stack"),

    # Grow/shrink windows left/right. 
    # This is mainly for the 'monadtall' and 'monadwide' layouts although it does also work in the 'bsp' and 'columns' layouts.
    Key([mod], "equal",
        lazy.layout.grow_left().when(layout=["bsp", "columns"]),
        lazy.layout.grow().when(layout=["monadtall", "monadwide"]),
        desc="Grow window to the left"
    ),
    Key([mod], "minus",
        lazy.layout.grow_right().when(layout=["bsp", "columns"]),
        lazy.layout.shrink().when(layout=["monadtall", "monadwide"]),
        desc="Grow window to the left"
    ),

    # Grow windows up, down, left, right.  Only works in certain layouts.
    # Works in 'bsp' and 'columns' layout.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key([mod], "m", lazy.layout.maximize(), desc='Toggle between min and max sizes'),
    Key([mod], "t", lazy.window.toggle_floating(), desc='toggle floating'),
    Key([mod], "f", maximize_by_switching_layout(), lazy.window.toggle_fullscreen(), desc='toggle fullscreen'),
    Key([mod, "shift"], "m", minimize_all(), desc="Toggle hide/show all windows on current group"),

    # Switch focus of monitors
    Key([altmod], "period", lazy.next_screen(), desc='Move focus to next monitor'),
    Key([altmod], "comma", lazy.prev_screen(), desc='Move focus to prev monitor'),
    
    # BRIGHTNESS
    # Need to ensure `brightnessctl` installed
    Key([], "XF86MonBrightnessDown", lazy.spawn('brightnessctl set 5%-'), desc="Down brightness"),
    Key([], "XF86MonBrightnessUp", lazy.spawn('brightnessctl set 5%+'), desc="Up brightness"), 

    # VOLUME
    Key([], "XF86AudioRaiseVolume", lazy.spawn('pactl set-sink-volume @DEFAULT_SINK@ +5%'), desc="Up the volume"),
    Key([], "XF86AudioLowerVolume", lazy.spawn('pactl set-sink-volume @DEFAULT_SINK@ -5%'), desc="Down the volume"),
    Key([], "XF86AudioMute", lazy.spawn('pactl set-sink-mute @DEFAULT_SINK@ toggle'), desc="Toggle mute"),
    Key([], "XF86AudioMicMute", lazy.spawn('pactl set-source-mute @DEFAULT_SOURCE@ toggle'), desc="Toggle mute the microphone"),

]

# --------------------------------------------------------
# GROUPS
# --------------------------------------------------------

groups = [
        Group("1", label='一', layout="columns"),
        Group("2", label='二', layout="columns"),
        Group("3", label='三', layout="columns"),
        Group("4", label='四', layout="columns"),
        Group("5", label='   ₅', layout="columns", matches=[Match(wm_class=["vivaldi-stable"])]),
        Group("6", label='   ₆', layout="columns", matches=[Match(wm_class=["Code"])]),
        Group("7", label='󰎛   ₇', layout="columns", matches=[Match(wm_class=["ONLYOFFICE Desktop Editors", "Joplin"])]),
        Group("8", label='󰍹   ₈', layout="columns", matches=[Match(wm_class=["Virt-manager"])]),
        Group("9", label='   ₉', layout="columns", matches=[Match(wm_class=["Doublecmd"])]),
]

for i in groups:
    keys.extend([

                Key([mod], i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name)),

                Key([mod, "shift"], i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(i.name)),

                ])

# --------------------------------------------------------
# LAYOUTS
# --------------------------------------------------------

layouts = [
    #layout.Bsp(**layout_theme),
    #layout.Floating(**layout_theme),
    #layout.RatioTile(**layout_theme),
    #layout.VerticalTile(**layout_theme),
    #layout.Matrix(**layout_theme),
    layout.MonadTall(
        margin = 4,
        border_width = 3,
        border_focus= colors[12],
        border_normal= colors[1]
        ),
    #layout.Spiral(**layout_theme),
    #layout.MonadWide(**layout_theme),
    layout.Tile(
        shift_windows=True,
        border_width = 3,
        margin = 4,
        ratio = 0.335,
        border_focus= colors[12],
        border_normal= colors[1]
         ),
    layout.Max(
         border_width = 0,
         margin = [0, 4, 0, 4]
         ),
    #layout.Stack(**layout_theme, num_stacks=2),
    layout.Columns(
        margin=[2, 4, 2, 4],
        border_width = 3,
        border_focus= colors[12],
        border_normal= colors[1]
    ),
    layout.TreeTab(
         font = "inter",
         fontsize = 11,
         section_fg = colors[4],
         sections = ["Windows:"],
         margin = [2, 4, 2, 4],
         border_width = 0,
         bg_color = colors[0],
         active_bg = colors[7],
         active_fg = colors[5],
         inactive_bg = colors[2],
         inactive_fg = colors[5],
         padding_left = 8,
         padding_x = 8,
         padding_y = 8,
         level_shift = 8,
         vspace = 3,
         panel_width = 140
         ),
    #layout.Zoomy(**layout_theme),
]

# --------------------------------------------------------
# WIDGETS
# --------------------------------------------------------

widget_defaults = dict(
    font="Inter, FontAwesome 6 Free Solid",
    fontsize = 10,
    padding = 0,
    background=colors[0]
)

extension_defaults = widget_defaults.copy()

def init_widgets_list():
    widgets_list = [
        widget.GroupBox(
                 font = "Inter Bold",
                 margin_y = 4,
                 margin_x = 4,
                 padding_y = 5,
                 padding_x = 8,
                 borderwidth = 3,
                 active = colors[3],
                 inactive = colors[4],
                 rounded = False,
                 highlight_color = colors[2],
                 highlight_method = "line",
                 urgent_alert_method = "text",
                 block_highlight_text_color = colors[5],
                 this_current_screen_border = colors[11],
                 this_screen_border = colors [4],
                 other_current_screen_border = colors[0],
                 other_screen_border = colors[0],
                 hide_unused=True,
                 urgent_text = colors[8],
                 ),
        widget.TextBox(
                 text = '|',
                 font = "Inter",
                 foreground = colors[4],
                 padding = 2,
                 fontsize = 10                 ),
        widget.CurrentLayoutIcon(
                 custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                 foreground = colors[6],
                 padding = 4,
                 scale = 0.6
                 ),
        widget.CurrentLayout(
                 foreground = colors[4],
                 padding = 5
                 ),
        widget.TextBox(
                 text = '|  ',
                 font = "Inter",
                 foreground = colors[4],
                 padding = 2,
                 fontsize = 10
                                  ),
        widget.WindowName(
                 foreground = colors[13],
                 max_chars = 40
                 ),
        widget.Spacer(length = 8),

        widget.Spacer(length = 8),
        
        # MEMORY

        widget.Memory(
                 foreground = colors[4],
                 mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
                 measure_mem='G',
                 format = '{MemUsed:.1f}{mm}',
                 fmt = '  Mem: {} ',
                 ),
        widget.Spacer(length = 12),

        # DISK SPACE

        widget.DF(
                 update_interval = 60,
                 foreground = colors[4],
                 mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e df')},
                 partition = '/',
                 #format = '[{p}] {uf}{m} ({r:.0f}%)',
                 format = '{uf}{m} free ',
                 fmt = '🖴  {}',
                 visible_on_warn = False,
                 ),
        widget.Spacer(length = 10),

        # BACKLIGH/BIRGHTNESS
        ## Ensure brightnessctl is installed
        widget.Backlight(
                foreground = colors[4],
                mouse_callbacks = {
                    "Button4": lambda: qtile.cmd_spawn("brightnessctl set +5%"),
                    "Button5": lambda: qtile.cmd_spawn("brightnessctl set 5%-"),
                },
                backlight_name="intel_backlight",
                format="󰃝   {percent:2.0%}",
            ),

        widget.Spacer(length = 10),
        
        # VOLUME
        ## If not working try installing `alsa-utils`

        widget.Volume(
                 foreground = colors[4],
                 fmt = '  {}  ',
                 ),
        widget.Spacer(length = 10),

       ## BATTERY
       #
       # widget.Battery(
       #         foreground = colors[11],
       #         format='  {percent:2.0%}  ',
       #          ),
       # widget.Spacer(length = 10),


        widget.Systray(padding = 3,
                foreground = colors[5],
                ),
        widget.Spacer(length = 10),

        # CLOCK

        widget.Clock(
                 font = "Inter Semi-Bold",
                 foreground = colors[5],
                 format = "%I:%M %p | %a %d %b",
                 fontsize = 10                
                 ),

        widget.Spacer(length = 10)
        ]
    return widgets_list

# --------------------------------------------------------
# SCREENS
# --------------------------------------------------------

def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1 

# Other monitors' bar will display everything but widgets on the right.
def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    del widgets_screen2[7:15]
    return widgets_screen2

# For adding transparency to your bar, add (background="#00000000") to the "Screen" line(s)
# For ex: Screen(top=bar.Bar(widgets=init_widgets_screen2(), background="#00000000", size=24)),

def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), margin=[2, 4, 2, 4], size=24)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), margin=[2, 4, 2, 4], size=26))]



if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()

def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)

def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)

def switch_screens(qtile):
    i = qtile.screens.index(qtile.current_screen)
    group = qtile.screens[i - 1].group
    qtile.current_screen.set_group(group)


# Move window to next screen
    
def window_to_previous_screen(qtile, switch_group=False, switch_screen=False):
    i = qtile.screens.index(qtile.current_screen)
    if i != 0:
        group = qtile.screens[i - 1].group.name
        qtile.current_window.togroup(group, switch_group=switch_group)
        if switch_screen == True:
            qtile.cmd_to_screen(i - 1)

def window_to_next_screen(qtile, switch_group=False, switch_screen=False):
    i = qtile.screens.index(qtile.current_screen)
    if i + 1 != len(qtile.screens):
        group = qtile.screens[i + 1].group.name
        qtile.current_window.togroup(group, switch_group=switch_group)
        if switch_screen == True:
            qtile.cmd_to_screen(i + 1)
        
keys.extend([
        Key([mod,"shift"],  "Right", lazy.function(window_to_next_screen)),
        Key([mod,"shift"],  "Left", lazy.function(window_to_previous_screen)),
        Key([mod,"control"],"Right", lazy.function(window_to_next_screen, switch_screen=True)),
        Key([mod,"control"],"Left", lazy.function(window_to_previous_screen, switch_screen=True)),
])

# --------------------------------------------------------
# GENERAL SETUP
# --------------------------------------------------------

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

# --------------------------------------------------------
# DEFINE FLOATING LAYOUTS
# --------------------------------------------------------

floating_layout = layout.Floating(
    border_focus=colors[14],
    border_width=2,
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="engrampa"),       # archive manager
        Match(wm_class="lxappearance"),   # lxappearance
        Match(wm_class="dialog"),         # dialog boxes
        Match(wm_class="download"),       # downloads
        Match(wm_class="error"),          # error msgs
        Match(wm_class="file_progress"),  # file progress boxes
        Match(wm_class="notification"),   # notifications
        Match(wm_class='pinentry-gtk-2'), # GPG key password entry
        Match(wm_class="toolbar"),        # toolbars
        Match(title='Confirmation'),      # tastyworks exit box
        Match(title="pinentry"),          # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "focus"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None



# --------------------------------------------------------
# AUTOSTART PROGRAMS
# --------------------------------------------------------

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])


# --------------------------------------------------------
# WINDOWS MANAGER NAME
# --------------------------------------------------------

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
