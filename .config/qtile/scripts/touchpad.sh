#!/bin/sh


# Set up the touchpad button mapping
xinput set-button-map 11 3 2 1 4 5 6 7

# Set up the logitec mouse
xinput set-button-map 9 3 2 1 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
