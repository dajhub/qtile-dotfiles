#!/usr/bin/env bash 

### AUTOSTART PROGRAMS ###

# Monitors (old Toshiba and External Acer)
xrandr --output LVDS-1 --primary --mode 1366X768 --rotate normal 
xrandr --output HDMI-1 --mode 1920x1080 --rotate normal --right-of LVDS-1


# Monitors (Lenova Ideapad 530S and External Acer)
#xrandr --output eDP-1 --primary --mode "1920x1080_60.00" 172.80  1920 2040 2248 2576  1080 1081 1084 1118  -HSync +Vsync --rotate normal 
#xrandr --output HDMI-1 --mode "1920x1080_60.00" 172.80  1920 2040 2248 2576  1080 1081 1084 1118  -HSync +Vsync --rotate normal --right-of eDP-1


sh ~/.config/qtile/scripts/touchpad.sh &


# Notifications
dunst &

# Load picom
picom --daemon &

# Network applet
nm-applet &

# Start xautolock using betterlockscreen
xautolock -time 10 -locker 'betterlockscreen --lock blur' &

# Wallpaper set with nitrogen
nitrogen --restore &
